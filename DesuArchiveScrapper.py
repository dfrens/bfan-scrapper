from sys import argv
from bs4 import BeautifulSoup
import urllib
import warnings
import requests
import json
import time
import io

startTime = time.time()
warnings.filterwarnings("ignore", category=UserWarning, module='bs4')
print("Starting DesuArchive scrapper...")

fileName = argv[3]
fileFormat = argv[4]
boardName = argv[1]
subjectName = argv[2]
encodedSubjectName = urllib.parse.quote(subjectName)

print(' Obtaining threads from board: "{}" with the subject name "{}"...'.format(boardName,subjectName))

r = requests.get("https://desuarchive.org/" + boardName + "/search/subject/" + subjectName + "/type/op/results/thread/order/asc/")
if r.status_code == 200:
    searchResult = BeautifulSoup(r.text, "html.parser")
    nextPage = searchResult.find('div', attrs={"class":"paginate"}).find('li', attrs={"class":"next"}).find('a').get('href')

    # iterate over the pages to obtain all the threads
    threadsListPage = []
    while nextPage != "#":
        if r.status_code == 200:
            searchResult = BeautifulSoup(r.text, "html.parser")
            threads = searchResult.find('aside', attrs={"class":"posts"}).find_all('article')
            currentPage = int(searchResult.find('div', attrs={"class":"paginate"}).find('li', attrs={"class":"active"}).get_text())
            nextPage = searchResult.find('div', attrs={"class":"paginate"}).find('li', attrs={"class":"next"}).find('a').get('href')
            threadsList = []
            for thread in threads:
                threadsList.append(thread.get('id'))
            threadsListPage.append(threadsList)
            if nextPage != "#":
                r = requests.get(nextPage)

# iterate over each thread to get the posts
    json.dumps(postsListPage)
    exit()


else:
    print(' The specified board was unreachable, stopping scrapper...')
print("     Scrapper ran for %s seconds." % (time.time() - startTime))