from sys import argv
from bs4 import BeautifulSoup
import warnings
import requests
import json
import time
import io
startTime = time.time()
warnings.filterwarnings("ignore", category=UserWarning, module='bs4')
print("Starting 8ch scrapper...")
fileName = argv[2]
fileFormat = argv[3]
boardName = argv[1]
print(' Obtaining threads from board: "{}"...'.format(boardName))
r = requests.get("http://8ch.net/" + boardName + "/threads.json")
if r.status_code == 200:
    board = json.loads(r.text)
    threadsList = []
    print("     {} pages obtained, parsing threads...".format(len(board)))
    for pages in board:
        for threads in pages['threads']:
            threadsList.append(threads['no'])
    print("         {} threads obtained, parsing posts...".format(len(threadsList)))
    postCount = 0
    newFile = io.open(fileName + "." + fileFormat,'w', encoding="utf-8")
    for threadNo in threadsList:
        t = requests.get("https://8ch.net/" + boardName + "/res/" + str(threadNo) + ".json")
        if t.status_code == 200:
            thread = json.loads(t.text)
            for posts in thread['posts']:
                if 'com' in posts:
                    soup = BeautifulSoup(posts['com'], "html.parser")
                    a_tags = soup.findAll('a')
                    for match in a_tags:
                        match.decompose()
                    strippedText = soup.get_text(" ")
                    if strippedText != " " and strippedText != "":
                        newFile.write("> " + strippedText + "\n")
                postCount += 1
    newFile.close()
    print("     {} posts parsed...".format(postCount))
    print('     Writing posts to file {}.{} process ended succesfully'.format(fileName,fileFormat))
else:
    print(' The specified board was unreachable, stopping scrapper...')
print("     Scrapper ran for %s seconds." % (time.time() - startTime))