# bfan-scrapper

Scraper to parse data from imageboard posts into a readable format to train a deep learning model

### Prerequisites

Before running, both **python** (^3.7) and the library **Beatiful Soup** (^4.4.0) should be installed, to install the library you can run:
```
pip install beautifulsoup4
```
If pip throws an incompatibility error, you should upgrade pip with:
```
python -m pip install --upgrade pip
```
### Running the scrapper
To run the 8ch scrapper:

```
python 8chScrapper.py board_name file_output_name file_output_format
```
To run the archive scrapper:
```
python DesuArchiveScrapper.py board_name subject_name file_output_name file_output_format
```
Replace the parameters with whatever you want, in the case of the subject in the **archive scrapper**, if it contains white spaces in it, add the parameter as:
```
python DesuArchiveScrapper.py board_name "subject name" file_output_name file_output_format
```

## Built With

* [Python 3.7.0](https://www.python.org/downloads/release/python-370/) - Language used
* [Beautiful Soup 4.4.0](https://www.crummy.com/software/BeautifulSoup/bs4) - Library for pulling data out of HTML and XML files

# TO-DO
- Finish work on archive scraper
- Refactorize code into classes
- Add comments to code
- Test other bs4 parsers for speed